class CreateOffices < ActiveRecord::Migration[5.2]
  def change
    create_table :offices do |t|
      t.string :name
      t.string :phone
      t.text :address
      t.references :state, foreign_key: {to_table: :locations}, null: false
      t.references :city, foreign_key: {to_table: :locations}, null: false

      t.timestamps
    end
  end
end
