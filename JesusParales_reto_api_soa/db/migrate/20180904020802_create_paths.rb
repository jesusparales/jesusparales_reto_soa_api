class CreatePaths < ActiveRecord::Migration[5.2]
  def change
    create_table :paths do |t|
      t.date :date
      t.date :arrival_date
      t.decimal :price, precision: 3, scale: 2
      t.integer :status
      t.references :shipping, foreign_key: true
      t.references :office, foreign_key: true

      t.timestamps
    end
  end
end
