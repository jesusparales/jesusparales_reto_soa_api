class LocationSerializer < ActiveModel::Serializer
  attributes :city_state_names

  def city_state_names
    "#{object.kind} #{object.name}"
  end

end
