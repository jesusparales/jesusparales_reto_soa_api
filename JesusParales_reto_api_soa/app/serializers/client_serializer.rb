class ClientSerializer < ActiveModel::Serializer
  attributes :ci, :full_name, :phone, :mobile

  #has_many :sent_shippings

   def full_name
     "#{object.name} #{object.lastname}"
   end
end
