class OfficeSerializer < ActiveModel::Serializer
  attributes :name, :phone, :address,:state_id, :city_id
end
