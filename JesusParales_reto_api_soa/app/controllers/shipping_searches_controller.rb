class ShippingSearchesController < ApplicationController

  # Con este método consultamos cualquier estatus del shipping indicado por parámetro con GET y Querystring
  def index
    @package = Shipping.where(status: params[:status])
    if @package
      render json: @package
    else
      render json: { error: 'Shipping Not Found' }
    end
  end

end
